import {get} from 'axios';
import {COINS} from 'Redux/constants/';

export default function getCoins() {
  return {
    type: COINS.GET,
    payload: get('https://api.coinmarketcap.com/v2/ticker/'),
  };
}
