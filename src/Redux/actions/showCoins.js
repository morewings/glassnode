import {COINS} from 'Redux/constants/';

export default function showCoins(amount) {
  return {
    type: COINS.SHOW,
    payload: Number(amount),
  };
}
