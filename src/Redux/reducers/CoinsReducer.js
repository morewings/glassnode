import {compose, sort, values} from 'ramda';
import {COINS} from 'Redux/constants';

const initialState = {
  data: [],
  amount: 100,
};

const sortCoins = compose(
  sort(({rank: rankA}, {rank: rankB}) => rankA - rankB),
  values,
);

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case `${COINS.GET}_FULFILLED`: {
      const data = sortCoins(action.payload.data.data);
      return {...state, data};
    }
    case COINS.SHOW: {
      return {...state, amount: action.payload};
    }
    default: {
      return state;
    }
  }
};

export default reducer;
