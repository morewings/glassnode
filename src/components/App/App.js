import React from 'react';
import {Container, Row, Col} from 'reactstrap';
import {Fragment} from 'redux-little-router';
import Navigation from 'components/Navigation/Navigation';
import MainPage from 'components/MainPage/MainPage';
import LiquidityPage from 'components/LiquidityPage/LiquidityPage';
import 'components/App/index.css';

const App = props => (
  <Fragment forRoute="/">
    <React.Fragment>
      <Container>
        <Row>
          <Col>
            <Navigation />
          </Col>
        </Row>
        <Row>
          <Col>
            <Fragment forRoute="/">
              <MainPage />
            </Fragment>
            <Fragment forRoute="/liquidity">
              <LiquidityPage />
            </Fragment>
          </Col>
        </Row>
      </Container>
    </React.Fragment>
  </Fragment>
);

export default App;
