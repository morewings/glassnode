import {connect} from 'react-redux';
import {slice, compose, curry, map} from 'ramda';
import Chart from 'components/Chart/Chart';

const filterData = curry(
  ({
    name,
    quotes: {
      USD: {
        percent_change_24h: priceChange,
        market_cap: marketCap,
        volume_24h: volume,
      },
    },
  }) => ({
    x: marketCap,
    y: volume,
    z: priceChange,
    name,
  }),
);

const mapStateToProps = state => ({
  coinsData: compose(
    map(filterData),
    slice(0),
  )(state.coins.amount, state.coins.data),
});

export default connect(mapStateToProps)(Chart);
