import React from 'react';
import PropTypes from 'prop-types';

const Chart = props => (
  <div>
    <div>
      Chart should be here. But I have not found proper library yet
      <span aria-label="bad me" role="img">
        😳
      </span>.
    </div>
    <pre className="pre-scrollable">
      <code>{JSON.stringify({coinsData: props.coinsData}, null, 2)}</code>
    </pre>
  </div>
);

Chart.propTypes = {
  coinsData: PropTypes.array.isRequired, // eslint-disable-line
};

export default Chart;
