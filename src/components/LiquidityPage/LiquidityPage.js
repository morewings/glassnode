import React from 'react';
import {Col, Row} from 'reactstrap';
import Slicer from 'components/Slicer';
import Chart from 'components/Chart';

const LiquidityPage = props => (
  <div>
    <Row>
      <Col>
        <h1>Liquidity analysis</h1>
      </Col>
    </Row>
    <Row>
      <Col>
        <Slicer />
      </Col>
    </Row>
    <Row>
      <Col>
        <Chart />
      </Col>
    </Row>
  </div>
);

export default LiquidityPage;
