import React from 'react';
import {Row, Col} from 'reactstrap';
import Ticker from 'components/Ticker';
import Slicer from 'components/Slicer';

const MainPage = props => (
  <div>
    <Row>
      <Col>
        <h1>Market overview</h1>
      </Col>
    </Row>
    <Row>
      <Col>
        <Slicer />
      </Col>
    </Row>
    <Row>
      <Col>
        <Ticker />
      </Col>
    </Row>
  </div>
);

export default MainPage;
