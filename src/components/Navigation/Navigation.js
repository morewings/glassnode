import React from 'react';
import {Link} from 'redux-little-router';
import {Navbar, Nav, NavItem} from 'reactstrap';

const Navigation = props => (
  <div>
    <Navbar color="light" light expand="md">
      <span className="navbar-brand">Glassnode</span>
      <Nav navbar>
        <NavItem>
          <Link className="nav-link" href="/">
            Market overview
          </Link>
        </NavItem>
        <NavItem>
          <Link className="nav-link" href="/liquidity">
            Liquidity analysis page
          </Link>
        </NavItem>
      </Nav>
    </Navbar>
  </div>
);

export default Navigation;
