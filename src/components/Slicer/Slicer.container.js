import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {showCoins} from 'Redux/actions';
import Slicer from 'components/Slicer/Slicer';

const mapStateToProps = state => ({
  amount: state.coins.amount,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({showCoins}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Slicer);
