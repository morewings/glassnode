import React from 'react';
import PropTypes from 'prop-types';
import {Form, FormGroup, Label, Input} from 'reactstrap';

const Slicer = props => (
  <div>
    <Form>
      <FormGroup tag="fieldset">
        <legend>Show me</legend>
        <FormGroup check>
          <Label check>
            <Input
              onChange={e => {
                props.showCoins(e.target.value);
              }}
              checked={props.amount === 10}
              type="radio"
              name="radio1"
              value={10}
            />
            10 entries
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              onChange={e => {
                props.showCoins(e.target.value);
              }}
              checked={props.amount === 50}
              type="radio"
              name="radio1"
              value={50}
            />
            50 entries
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              onChange={e => {
                props.showCoins(e.target.value);
              }}
              checked={props.amount === 100}
              type="radio"
              name="radio1"
              value={100}
            />
            100 entries
          </Label>
        </FormGroup>
        <FormGroup check>
          <Label check>
            <Input
              onChange={e => {
                props.showCoins(e.target.value);
              }}
              checked={props.amount === 1000}
              type="radio"
              name="radio1"
              value={1000}
            />{/* Unlimited amounts are unsafe, when we are working with external API */}
            All entries
          </Label>
        </FormGroup>
      </FormGroup>
    </Form>
  </div>
);

Slicer.propTypes = {
  showCoins: PropTypes.func.isRequired,
  amount: PropTypes.number.isRequired,
};

export default Slicer;
