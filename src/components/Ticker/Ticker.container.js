import {connect} from 'react-redux';
import {slice, compose, curry, map} from 'ramda';
import Ticker from 'components/Ticker/Ticker';

const filterData = curry(
  ({
    id: key,
    name,
    rank,
    quotes: {
      USD: {
        price,
        percent_change_24h: priceChange,
        market_cap: marketCap,
        volume_24h: volume,
      },
    },
  }) => ({
    key,
    name,
    rank,
    price,
    priceChange,
    marketCap,
    volume,
  }),
);

const mapStateToProps = state => ({
  coinsData: compose(
    map(filterData),
    slice(0),
  )(state.coins.amount, state.coins.data),
});

export default connect(mapStateToProps)(Ticker);
