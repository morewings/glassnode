import React from 'react';
import PropTypes from 'prop-types';
import {Table} from 'reactstrap';
import 'components/Ticker/Ticker.css';

const Row = props => (
  <tr>
    <td className="numberCell">{props.rank}</td>
    <td>{props.name}</td>
    <td className="numberCell">${props.price}</td>
    <td className="numberCell">{props.priceChange}%</td>
    <td className="numberCell">${props.marketCap}</td>
    <td className="numberCell">${props.volume}</td>
  </tr>
);

Row.propTypes = {
  rank: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  price: PropTypes.number.isRequired,
  priceChange: PropTypes.number.isRequired,
  marketCap: PropTypes.number.isRequired,
  volume: PropTypes.number.isRequired,
};

const Ticker = props => (
  <div className="table-responsive">
    <Table>
      <thead>
        <tr>
          <th className="numberCell">Rank</th>
          <th>Name</th>
          <th className="numberCell">Price</th>
          <th className="numberCell">Price Change (24h)</th>
          <th className="numberCell">Market Cap</th>
          <th className="numberCell">Volume (24h)</th>
        </tr>
      </thead>
      <tbody>{props.coinsData.map(coinData => <Row {...coinData} />)}</tbody>
    </Table>
  </div>
);

Ticker.propTypes = {
  coinsData: PropTypes.array.isRequired, // eslint-disable-line
};

export default Ticker;
