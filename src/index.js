import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, compose, combineReducers} from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import {routerForBrowser, initializeCurrentLocation} from 'redux-little-router';
import routes from 'routes';
import {coins} from 'Redux/reducers';
import {getCoins} from 'Redux/actions';
import App from 'components/App/App';

const {reducer, middleware, enhancer} = routerForBrowser({
  routes,
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose; // eslint-disable-line

const store = createStore(
  combineReducers({router: reducer, coins}),
  composeEnhancers(enhancer, applyMiddleware(middleware, promiseMiddleware())),
);

const initialLocation = store.getState().router;
if (initialLocation) {
  store.dispatch(initializeCurrentLocation(initialLocation));
  store.dispatch(getCoins());
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'),
);
