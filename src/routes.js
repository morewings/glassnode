export default {
  '/': {
    title: 'Market overview',
  },
  '/liquidity': {
    title: 'Liquidity analysis page',
  },
};
